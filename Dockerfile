FROM busybox
COPY docker-entrypoint.sh /

CMD [ "/docker-entrypoint.sh" ]